﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : Singleton<Gravity>
{
    #region Fiels
    public float m_planetRadius;
    public float m_gravityDistanceLimit;
    #endregion

    #region Constructor
    protected Gravity()
    {
        //Protected Constructor to prevent non-singleton constructor use.
    }
    #endregion

    #region Monobehaviour methods
    void Start()
    {
    }

    void Update()
    {
    }
    #endregion

    #region Public Gravity Methods
    public Vector2 GetGravityVector(float x_position, float y_position, float mass)
    {
        Vector2 result = new Vector2(-x_position, -y_position);
        result.Normalize();
        float distance = Mathf.Sqrt(Mathf.Pow(x_position, 2) + Mathf.Pow(y_position, 2));
        return result * mass * m_gravityDistanceLimit / distance;
    }

    public float GetGravityDistanceLimit()
    {
        return m_gravityDistanceLimit;
    }

    public void SetGravityDistanceLimit(float distance)
    {
        m_gravityDistanceLimit = distance;
    }

    public float GetPlanetRadius()
    {
        return m_planetRadius;
    }

    public void SetPlanetRadius(float radius)
    {
        m_planetRadius = radius;
    }
    #endregion

}
