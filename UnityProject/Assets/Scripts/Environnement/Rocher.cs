﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocher : MonoBehaviour
{
    private SpriteRenderer m_SpriteRenderer;
    private PolygonCollider2D m_Collider;
    private Rigidbody2D m_RigidBody;
    private Transform m_Transform;

    public Item m_ItemHidden;
    public GameObject m_Player1;
    public GameObject m_Player2;

    private Transform m_TransformPlayer1;
    private Transform m_TransformPlayer2;
    private DateTime m_LastTimeHitByPlayer1;
    private DateTime m_LastTimeHitByPlayer2;

    public void Start()
    {
        m_RigidBody = this.GetComponent<Rigidbody2D>();
        m_SpriteRenderer = this.GetComponent<SpriteRenderer>();
        m_Collider = this.GetComponent<PolygonCollider2D>();

        m_Transform = this.GetComponent<Transform>();
        m_TransformPlayer1 = m_Player1.GetComponent<Transform>();
        m_TransformPlayer2 = m_Player2.GetComponent<Transform>();

        m_SpriteRenderer.enabled = true;
        m_SpriteRenderer.sortingLayerName = "Player";
        m_Collider.enabled = true;
        m_Collider.isTrigger = false;
        m_LastTimeHitByPlayer1 = DateTime.MaxValue;
        m_LastTimeHitByPlayer2 = DateTime.MaxValue;
    }

    public void Update()
    {
        if (m_ItemHidden != null && m_ItemHidden.IsCollected())
        {
            m_Collider.enabled = false;
            m_SpriteRenderer.sortingLayerName = "UnderItems";
            m_RigidBody.simulated = false;
        }
        else
        {
            ApplyGravity();
            double nb_milliseconds_passed_player1 = (DateTime.Now - m_LastTimeHitByPlayer1).TotalMilliseconds;
            double nb_milliseconds_passed_player2 = (DateTime.Now - m_LastTimeHitByPlayer2).TotalMilliseconds;
            if (nb_milliseconds_passed_player1 > 0 && nb_milliseconds_passed_player2 > 0 && nb_milliseconds_passed_player1 < 1000 && nb_milliseconds_passed_player2 < 1000)
            {
                float dtime = Time.deltaTime;
                if (m_TransformPlayer1.position.x < m_Transform.position.x && m_TransformPlayer2.position.x < m_Transform.position.x)
                    m_Transform.Translate(0, -dtime, 0);
                else if (m_TransformPlayer1.position.x > m_Transform.position.x && m_TransformPlayer2.position.x > m_Transform.position.x)
                    m_Transform.Translate(0, +dtime, 0);
            }
        }
    }

    protected void ApplyGravity()
    {
        float x_position = this.transform.position.x;
        float y_position = this.transform.position.y;
        m_RigidBody.AddForce(Gravity.Instance.GetGravityVector(x_position, y_position, m_RigidBody.mass));
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            string name = collision.gameObject.name;
            if (name.Equals("Player1"))
            {
                m_LastTimeHitByPlayer1 = DateTime.Now;
            }
            if (name.Equals("Player2"))
            {
                m_LastTimeHitByPlayer2 = DateTime.Now;
            }
        }
    }
}
