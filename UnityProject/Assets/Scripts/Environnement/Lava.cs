﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    private bool m_IsFreeze = false;
    private DateTime m_LastTimeHitByParticle;
    public Animator m_Animator;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        DateTime now = DateTime.Now;
        double time = (now - m_LastTimeHitByParticle).TotalSeconds;
        if (time > 1)
        {
            m_IsFreeze = false;
            m_Animator.SetBool("m_Frozen", false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!m_IsFreeze && collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerControler>().SetIsDeadByLava(true);
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.CompareTag("Extinguisher"))
        {
            m_LastTimeHitByParticle = DateTime.Now;
            m_IsFreeze = true;
            m_Animator.SetBool("m_Frozen", true);
        }
    }
}
