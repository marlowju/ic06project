﻿using System;
using UnityEngine;

public class Cave : MonoBehaviour
{
    #region Fields
    private DateTime m_LastTimeHitByParticle;
    private BoxCollider2D m_BoxCollider;
    public Item m_ItemHidden;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        m_BoxCollider = this.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        DateTime now = DateTime.Now;
        double time = (now - m_LastTimeHitByParticle).TotalSeconds;
        if (time > 1)
        {
            m_ItemHidden.SetCollectable(false);
            m_BoxCollider.isTrigger = false;
        }
    }

    public void HitByParticle()
    {
        m_LastTimeHitByParticle = DateTime.Now;
        m_ItemHidden.SetCollectable(true);
    }
}
