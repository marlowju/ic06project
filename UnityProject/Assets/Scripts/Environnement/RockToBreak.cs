﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockToBreak : MonoBehaviour
{
    private SpriteRenderer m_SpriteRenderer;
    private PolygonCollider2D m_Collider;

    public void Start()
    {
        m_SpriteRenderer = this.GetComponent<SpriteRenderer>();
        m_Collider = this.GetComponent<PolygonCollider2D>();

        m_SpriteRenderer.enabled = true;
        m_SpriteRenderer.sortingLayerName = "Player";
        m_Collider.enabled = true;
        m_Collider.isTrigger = false;
    }

    public void Update()
    {

    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerControler playerControler = collision.gameObject.GetComponent<PlayerControler>();
            if (playerControler.IsHoldingPickaxe() && Input.GetKey(KeyCode.O))
            {
                m_Collider.enabled = false;
                m_SpriteRenderer.sortingLayerName = "UnderItems";
            }
        }
    }
}
