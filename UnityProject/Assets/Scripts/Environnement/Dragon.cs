﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : MonoBehaviour
{
    private ParticleSystem m_FireParticleSystem;
    private bool m_IsCalm = false;
    private List<PlayerControler> m_PlayersDetected;
    public PlayerControler m_Player1;
    public PlayerControler m_Player2;

    // Start is called before the first frame update
    void Start()
    {
        m_FireParticleSystem = GetComponentInChildren<ParticleSystem>();
        m_FireParticleSystem.Stop();
        m_PlayersDetected = new List<PlayerControler>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        bool isOnePlayerDetectedHoldingJewel = false;
        foreach (PlayerControler player in m_PlayersDetected)
        {
            if (player.IsHoldingJewel())
            {
                isOnePlayerDetectedHoldingJewel = true;
                break;
            }   
        }
        m_IsCalm = isOnePlayerDetectedHoldingJewel;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            m_PlayersDetected.Add(collision.gameObject.GetComponent<PlayerControler>());
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!m_IsCalm && !m_FireParticleSystem.isPlaying)
            m_FireParticleSystem.Play();
        else if (m_IsCalm && m_FireParticleSystem.isPlaying)
            m_FireParticleSystem.Stop();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            foreach (PlayerControler player in m_PlayersDetected)
            {
                if (player.gameObject.name.Equals(collision.gameObject.name))
                {
                    m_PlayersDetected.Remove(player);
                    break;
                }
            }
        }

        if (m_PlayersDetected.Count == 0 && m_FireParticleSystem.isPlaying)
            m_FireParticleSystem.Stop();

    }
}
