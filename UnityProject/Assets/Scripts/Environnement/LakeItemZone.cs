﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LakeItemZone : MonoBehaviour
{
    private GameObject m_PlayerCurrentlyBreakingLake = null;
    private PlayerControler m_PlayerCurrentlyBreakingLake_PlayerControler = null;

    private bool m_HasBroke = false;
    private int m_TimeToBreak_MilliSeconds = 5000;

    public Item m_ItemHidden;
    public Animator m_LakeBrokenAnimator;
    private Animator m_ItemHiddenAnimator;
    private SpriteRenderer m_BrokenLakeSpriteRenderer;

    private AudioSource m_AudioSource;
    public AudioClip m_IceBreakingSound;

    // Start is called before the first frame update
    void Start()
    {
        m_ItemHiddenAnimator = m_ItemHidden.gameObject.GetComponent<Animator>();
        m_ItemHiddenAnimator.SetBool("m_OutOfIce", false);
        m_BrokenLakeSpriteRenderer = this.GetComponent<SpriteRenderer>();
        m_BrokenLakeSpriteRenderer.enabled = false;
        m_AudioSource = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!m_HasBroke && collision.gameObject.CompareTag("Player"))
        {
            m_PlayerCurrentlyBreakingLake = collision.gameObject;
            m_PlayerCurrentlyBreakingLake_PlayerControler = m_PlayerCurrentlyBreakingLake.GetComponent<PlayerControler>();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!m_HasBroke)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                if (m_PlayerCurrentlyBreakingLake != null && m_PlayerCurrentlyBreakingLake_PlayerControler != null && m_PlayerCurrentlyBreakingLake_PlayerControler.IsHoldingPickaxe() && Input.GetKey(KeyCode.O))
                {
                    float dt = Time.deltaTime;
                    m_TimeToBreak_MilliSeconds = Mathf.Max(m_TimeToBreak_MilliSeconds - (int)(dt * 1000), 0);
                }

                int state = m_LakeBrokenAnimator.GetInteger("State");
                if (state * 1000 > m_TimeToBreak_MilliSeconds)
                {
                    m_LakeBrokenAnimator.SetInteger("State", state - 1);
                    PlayIceBreakingSound();
                }

                if (m_TimeToBreak_MilliSeconds == 0)
                {
                    m_HasBroke = true;
                    m_BrokenLakeSpriteRenderer.enabled = true;
                    m_ItemHiddenAnimator.SetBool("m_OutOfIce", true);
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && (m_PlayerCurrentlyBreakingLake != null || m_PlayerCurrentlyBreakingLake_PlayerControler != null))
        {
            m_PlayerCurrentlyBreakingLake = null;
            m_PlayerCurrentlyBreakingLake_PlayerControler = null;
        }
    }

    private void PlayIceBreakingSound()
    {
        if (m_AudioSource != null && m_IceBreakingSound != null)
        {
            m_AudioSource.clip = m_IceBreakingSound;
            m_AudioSource.Play();
        }
    }
}
