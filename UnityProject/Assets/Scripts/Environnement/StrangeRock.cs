﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrangeRock : MonoBehaviour
{
    public Cave m_Cave;
    private BoxCollider2D m_CaveBoxCollider;
    private ParticleSystem m_ParticleSystem;
    private List<ParticleSystem.Particle> enter = new List<ParticleSystem.Particle>();


    // Start is called before the first frame update
    void Start()
    {
        m_ParticleSystem = this.GetComponent<ParticleSystem>();
        m_CaveBoxCollider = m_Cave.GetComponent<BoxCollider2D>();
        m_ParticleSystem.trigger.SetCollider(1, m_CaveBoxCollider);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnParticleTrigger()
    {
        int numEnter = m_ParticleSystem.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);

        if (numEnter != 0)
        {
            m_CaveBoxCollider.isTrigger = true;
            m_Cave.HitByParticle();
        }

        /*
        for (int i = 0; i < numEnter; i++)
        {
            ParticleSystem.Particle p = enter[i];
            //take damage
            enter[i] = p;
        }

        m_ParticleSystem.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
        */
    }
}
