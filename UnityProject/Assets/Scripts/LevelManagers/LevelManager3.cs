﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager3 : MonoBehaviour
{
    #region Fields
    public GameObject m_Player1;
    public GameObject m_Player2;
    public ItemManager m_ItemManager;

    public ParticleSystem m_ShipSmoke1;
    public ParticleSystem m_ShipSmoke2;

    private bool m_GoToNextLevel = false;
    private bool m_GoBackToMainMenu = false;
    private bool m_Reset = false;

    private PlayerControler m_Player1_playerControler;
    private PlayerControler m_Player2_playerControler;

    #region UI management
    public GameObject m_ObjectivePanel;
    public GameObject m_ItemRopePanel;
    public GameObject m_ItemMirrorPanel;
    public GameObject m_ItemPickaxePanel;
    public GameObject m_ItemAzotePanel;
    public GameObject m_ItemExplosifPanel;
    public GameObject m_ItemExtinguisherPanel;
    public GameObject m_ItemJewelPanel;
    public GameObject m_ItemShipPiecePanel;
    public GameObject m_DefeatPanel;
    public Text m_DefeatPanel_Text;
    public GameObject m_VictoryPanel;
    public GameObject m_InventoryPanel;

    public Button m_MenuButton;
    public GameObject m_MenuPanel;
    #endregion
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        m_Player1_playerControler = m_Player1.GetComponent<PlayerControler>();
        m_Player2_playerControler = m_Player2.GetComponent<PlayerControler>();
        
        m_ObjectivePanel.SetActive(true);

        m_ItemRopePanel.SetActive(false);
        m_ItemShipPiecePanel.SetActive(false);
        m_DefeatPanel.SetActive(false);
        m_VictoryPanel.SetActive(false);
        m_InventoryPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //Defeat 1 : roasted
        if (m_Player1_playerControler.IsRoasted() || m_Player2_playerControler.IsRoasted())
        {
            if (m_Player1_playerControler.IsRoasted() && m_Player2_playerControler.IsRoasted())
            {
                m_DefeatPanel_Text.text = "Player 1 and Player 2 have been roasted to death...";
            }
            else if (m_Player1_playerControler.IsRoasted())
            {
                m_DefeatPanel_Text.text = "Player 1 has been roasted to death...";
            }
            else
            {
                m_DefeatPanel_Text.text = "Player 2 has been roasted to death...";
            }
            m_DefeatPanel.SetActive(true);
            m_Player1.gameObject.SetActive(false);
            m_Player2.gameObject.SetActive(false);
        }
        //Defeat 2 : burnt
        if (m_Player1_playerControler.IsBurned() || m_Player2_playerControler.IsBurned())
        {
            if (m_Player1_playerControler.IsBurned() && m_Player2_playerControler.IsBurned())
            {
                m_DefeatPanel_Text.text = "Player 1 and Player 2 have been burned to death...";
            }
            else if (m_Player1_playerControler.IsBurned())
            {
                m_DefeatPanel_Text.text = "Player 1 has been burned to death...";
            }
            else
            {
                m_DefeatPanel_Text.text = "Player 2 has been burned to death...";
            }
            m_DefeatPanel.SetActive(true);
            m_Player1.gameObject.SetActive(false);
            m_Player2.gameObject.SetActive(false);
        }

        //Defeat 3 : lava
        if (m_Player1_playerControler.IsDeadByLava() || m_Player2_playerControler.IsDeadByLava())
        {
            if (m_Player1_playerControler.IsDeadByLava() && m_Player2_playerControler.IsDeadByLava())
            {
                m_DefeatPanel_Text.text = "Player 1 and Player 2 have touched lava and died...";
            }
            else if (m_Player1_playerControler.IsDeadByLava())
            {
                m_DefeatPanel_Text.text = "Player 1 has touched lava and died...";
            }
            else
            {
                m_DefeatPanel_Text.text = "Player 2 has touched lava and died...";
            }
            m_DefeatPanel.SetActive(true);
            m_Player1.gameObject.SetActive(false);
            m_Player2.gameObject.SetActive(false);
        }

        //Victory
        if (m_ItemManager.HasTropheeBeenCollected() && m_Player1_playerControler.IsCloseToShip() && m_Player2_playerControler.IsCloseToShip())
        {
            m_ShipSmoke1.Stop();
            m_ShipSmoke2.Stop();
            m_VictoryPanel.SetActive(true);
        }
    }

    #region Public accessors
    public bool GoToNextLevel()
    {
        return m_GoToNextLevel;
    }

    public bool Reset()
    {
        return m_Reset;
    }

    public bool GoBackToMainMenu()
    {
        return m_GoBackToMainMenu;
    }
    #endregion

    #region UI bindings
    public void onObjectiveButtonClick()
    {
        m_ObjectivePanel.SetActive(false);
    }

    public void onRopeButtonClick()
    {
        m_ItemRopePanel.SetActive(false);
    }

    public void onMirrorButtonClick()
    {
        m_ItemMirrorPanel.SetActive(false);
    }

    public void onPickaxeButtonClick()
    {
        m_ItemPickaxePanel.SetActive(false);
    }

    public void onAzoteButtonClick()
    {
        m_ItemAzotePanel.SetActive(false);
    }

    public void onShipPieceButtonClick()
    {
        m_ItemShipPiecePanel.SetActive(false);
    }

    public void onExplosifButtonClick()
    {
        m_ItemExplosifPanel.SetActive(false);
    }

    public void onExtinguisherfButtonClick()
    {
        m_ItemExtinguisherPanel.SetActive(false);
    }

    public void onJewelfButtonClick()
    {
        m_ItemJewelPanel.SetActive(false);
    }

    public void onMenuButtonClick()
    {
        m_MenuPanel.SetActive(true);
    }

    public void onReturnButtonClick()
    {
        m_MenuPanel.SetActive(false);
    }

    public void OnMainMenuButtonClick()
    {
        m_GoBackToMainMenu = true;
    }

    public void OnResetButtonClick()
    {
        m_Reset = true;
    }

    public void onGoToNextLevelButtonClick()
    {
        m_GoToNextLevel = true;
    }

    public void onInventoryClick()
    {
        m_InventoryPanel.SetActive(true);
        m_MenuPanel.SetActive(false);
    }
    #endregion
}
