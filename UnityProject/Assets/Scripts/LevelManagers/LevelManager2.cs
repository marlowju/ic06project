﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager2 : MonoBehaviour
{
    #region Fields
    public GameObject m_Player1;
    public GameObject m_Player2;
    public ItemManager m_ItemManager;

    public ParticleSystem m_ShipSmoke1;
    public ParticleSystem m_ShipSmoke2;

    private bool m_GoToNextLevel = false;
    private bool m_GoBackToMainMenu = false;
    private bool m_Reset = false;

    private PlayerControler m_Player1_playerControler;
    private PlayerControler m_Player2_playerControler;

    #region UI management
    public GameObject m_ObjectivePanel;
    public GameObject m_ItemRopePanel;
    public GameObject m_ItemMirrorPanel;
    public GameObject m_ItemPickaxePanel;
    public GameObject m_ItemOxygenPanel;
    public GameObject m_ItemShipPiecePanel;
    public GameObject m_DefeatPanel;
    public Text m_DefeatPanel_Text;
    public GameObject m_VictoryPanel;
    public GameObject m_InventoryPanel;

    public Button m_MenuButton;
    public GameObject m_MenuPanel;
    #endregion
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        m_Player1_playerControler = m_Player1.GetComponent<PlayerControler>();
        m_Player2_playerControler = m_Player2.GetComponent<PlayerControler>();
        
        m_ObjectivePanel.SetActive(true);

        m_ItemRopePanel.SetActive(false);
        m_ItemShipPiecePanel.SetActive(false);
        m_DefeatPanel.SetActive(false);
        m_VictoryPanel.SetActive(false);
        m_InventoryPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //Defeat 1 : drown
        if (m_Player1_playerControler.IsDrown() || m_Player2_playerControler.IsDrown())
        {
            if (m_Player1_playerControler.IsDrown() && m_Player2_playerControler.IsDrown())
            {
                m_DefeatPanel_Text.text = "Player 1 and Player 2 drowned...";
            }
            else if (m_Player1_playerControler.IsDrown())
            {
                m_DefeatPanel_Text.text = "Player 1 drowned...";
            }
            else
            {
                m_DefeatPanel_Text.text = "Player 2 drowned...";
            }
            m_DefeatPanel.SetActive(true);
            m_Player1.gameObject.SetActive(false);
            m_Player2.gameObject.SetActive(false);
        }

        //Victory
        if (m_ItemManager.HasTropheeBeenCollected() && m_Player1_playerControler.IsCloseToShip() && m_Player2_playerControler.IsCloseToShip())
        {
            m_ShipSmoke1.Stop();
            m_ShipSmoke2.Stop();
            m_VictoryPanel.SetActive(true);
        }
    }

    #region Public accessors
    public bool GoToNextLevel()
    {
        return m_GoToNextLevel;
    }

    public bool Reset()
    {
        return m_Reset;
    }

    public bool GoBackToMainMenu()
    {
        return m_GoBackToMainMenu;
    }
    #endregion

    #region UI bindings
    public void onObjectiveButtonClick()
    {
        m_ObjectivePanel.SetActive(false);
    }

    public void onRopeButtonClick()
    {
        m_ItemRopePanel.SetActive(false);
    }

    public void onMirrorButtonClick()
    {
        m_ItemMirrorPanel.SetActive(false);
    }

    public void onPickaxeButtonClick()
    {
        m_ItemPickaxePanel.SetActive(false);
    }

    public void onOxygenButtonClick()
    {
        m_ItemOxygenPanel.SetActive(false);
    }

    public void onShipPieceButtonClick()
    {
        m_ItemShipPiecePanel.SetActive(false);
    }

    public void onMenuButtonClick()
    {
        m_MenuPanel.SetActive(true);
    }

    public void onReturnButtonClick()
    {
        m_MenuPanel.SetActive(false);
    }

    public void OnMainMenuButtonClick()
    {
        m_GoBackToMainMenu = true;
    }

    public void OnResetButtonClick()
    {
        m_Reset = true;
    }

    public void onGoToNextLevelButtonClick()
    {
        m_GoToNextLevel = true;
    }

    public void onInventoryClick()
    {
        m_InventoryPanel.SetActive(true);
        m_MenuPanel.SetActive(false);
    }
    #endregion
}
