﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    private List<Item> m_ItemListToShow;
    private int m_ItemCurrentlyShowedIndice = 0;

    public GameObject m_InventoryPanel;
    public Text m_ItemName;
    public Text m_ItemUse;
    public Text m_ItemHowToUse;
    public Image m_ItemView;
    public Image m_ItemKey1;
    public Image m_ItemKey2;
    public Text m_ItemSingleOrMultipleUse;

    // Start is called before the first frame update
    void Start()
    {
        m_InventoryPanel.gameObject.SetActive(false);
        if (m_ItemListToShow == null)
            m_ItemListToShow = new List<Item>();
        m_ItemName.text = "";
        m_ItemHowToUse.text = "";
        m_ItemView.sprite = null;
        m_ItemKey1.sprite = null;
        m_ItemKey2.sprite = null;
        m_ItemSingleOrMultipleUse.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (m_ItemListToShow.Count == 0)
            m_ItemName.text = "Your inventory is empty...";
        else
        {
            Item item = m_ItemListToShow[m_ItemCurrentlyShowedIndice];
            m_ItemName.text = item.m_ItemName;
            m_ItemUse.text = item.m_ItemUse;
            m_ItemHowToUse.text = item.m_ItemHowToUse;
            m_ItemView.sprite = item.m_ItemImage;
            m_ItemKey1.sprite = item.m_ItemKey1;
            m_ItemKey2.sprite = item.m_ItemKey2;
            m_ItemSingleOrMultipleUse.text = item.m_ItemSingleOrMultipleUse;
        }
    }

    #region Public methods
    public void AddItemToInventory(Item item)
    {
        if (m_ItemListToShow == null)
            m_ItemListToShow = new List<Item>();
        m_ItemListToShow.Add(item);
    }
    #endregion

    #region UI management
    public void OnButtonLastClick()
    {
        m_ItemCurrentlyShowedIndice--;
        if (m_ItemCurrentlyShowedIndice < 0)
            m_ItemCurrentlyShowedIndice = m_ItemListToShow.Count -1;
    }

    public void OnButtonNextClick()
    {
        m_ItemCurrentlyShowedIndice++;
        if (m_ItemCurrentlyShowedIndice > m_ItemListToShow.Count - 1)
            m_ItemCurrentlyShowedIndice = 0;
    }

    public void onButtonCloseClick()
    {
        m_InventoryPanel.gameObject.SetActive(false);
    }
    #endregion
}
