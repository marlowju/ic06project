﻿using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.ParticleSystem;

public abstract class Item : MonoBehaviour
{
    #region Fields
    protected bool m_Collectable;
    protected bool m_Collected;
    protected SpriteRenderer m_SpriteRenderer;
    protected CircleCollider2D m_CircleCollider;
    protected ParticleSystem m_ParticleSystem;
    protected EmissionModule m_PSEmissionModule;
    public GameObject m_ExplicationPanel;

    protected AudioSource m_AudioSource;
    [Header("Sounds")]
    public AudioClip m_CollectedSound;
    public AudioClip m_UsedSound;

    [Header("UI Settings")]
    public InventoryManager m_InventoryManager;
    public Sprite m_ItemImage;
    public System.String m_ItemName;
    public System.String m_ItemUse;
    public System.String m_ItemHowToUse;
    public Sprite m_ItemKey1;
    public Sprite m_ItemKey2;
    public System.String m_ItemSingleOrMultipleUse;
    #endregion

    #region Monobehaviour methods
    protected virtual void Start()
    {
        InitIsCollectable();
        m_Collected = false;
        m_SpriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        m_CircleCollider = this.gameObject.GetComponent<CircleCollider2D>();
        m_ParticleSystem = this.gameObject.GetComponent<ParticleSystem>();
        m_AudioSource = this.gameObject.GetComponent<AudioSource>();
        m_PSEmissionModule = m_ParticleSystem.emission;

        //Dealing with item rotation depending on position
        RotateItem();
    }

    protected virtual void Update()
    {
        if (m_Collectable && !m_Collected)
        {
            if (m_SpriteRenderer.enabled == false)
                m_SpriteRenderer.enabled = true;
            if (m_CircleCollider.enabled == false)
                m_CircleCollider.enabled = true;
            if (m_ParticleSystem.isPlaying == false)
                m_ParticleSystem.Play();
        }

        if (!m_Collectable || m_Collected)
        {
            if (m_SpriteRenderer.enabled == true)
                m_SpriteRenderer.enabled = false;
            if (m_CircleCollider.enabled == true)
                m_CircleCollider.enabled = false;
            if (m_ParticleSystem.isPlaying == true)
                m_ParticleSystem.Stop();
        }
    }
    #endregion

    #region Item collect methods
    protected void HasBeenCollected()
    {
        CheckComponentsNotNull();

        m_ExplicationPanel.SetActive(true);
        m_SpriteRenderer.enabled = false;
        m_CircleCollider.enabled = false;
        m_Collected = true;

        m_InventoryManager.AddItemToInventory(this);

        if (m_AudioSource != null && m_CollectedSound != null)
        {
            m_AudioSource.clip = m_CollectedSound;
            m_AudioSource.Play();
        }
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (!m_Collected && other.gameObject.CompareTag("Player"))
            this.HasBeenCollected();
    }

    public bool IsCollected()
    {
        return m_Collected;
    }

    public void SetCollectable(bool collectable)
    {
        m_Collectable = collectable;
    }
    #endregion

    #region Item virtual methods
    protected virtual void InitIsCollectable()
    {
        m_Collectable = true;
    }
    #endregion

    #region Item private methods
    protected void RotateItem()
    {
        float x_position = this.transform.position.x;
        float y_position = this.transform.position.y;

        float rotationAngle = Mathf.Atan(Mathf.Abs(x_position) / Mathf.Abs(y_position)) * Mathf.Rad2Deg;
        if (x_position > 0)
            rotationAngle = -rotationAngle;
        if (y_position < 0)
            rotationAngle = 180 - rotationAngle;

        Quaternion target = Quaternion.Euler(0, 0, rotationAngle);
        this.transform.rotation = target;
    }

    private void CheckComponentsNotNull()
    {
        if (m_SpriteRenderer == null)
            m_SpriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        if (m_CircleCollider == null)
            m_CircleCollider = this.gameObject.GetComponent<CircleCollider2D>();
        if (m_ParticleSystem == null)
            m_ParticleSystem = this.gameObject.GetComponent<ParticleSystem>();
        m_PSEmissionModule = m_ParticleSystem.emission;
    }
    #endregion

}
