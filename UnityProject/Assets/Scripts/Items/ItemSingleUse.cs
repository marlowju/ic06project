﻿using UnityEngine;

public abstract class ItemSingleUse : Item
{
    #region Fields
    protected bool m_Using;
    protected bool m_Used;

    protected bool m_UseOnOnePlayer = false;
    protected GameObject m_PlayerSelected;
    public GameObject m_SelectionPlayerPanel;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();
        m_Using = false;
        m_Used = false;
        Set_UseOnOnePlayer();
        if (m_UseOnOnePlayer && m_SelectionPlayerPanel != null)
            m_SelectionPlayerPanel.SetActive(false);
        m_ItemSingleOrMultipleUse = "Single use";
    }
    
    protected override void Update()
    {
        base.Update();
        if (m_Collected && !m_Used)
        {
            if (!m_Using)
            {
                if (Input.GetKey(GetUseKeyCode()) && CheckUsingItemAllowed())
                {
                    if (m_UseOnOnePlayer && m_PlayerSelected == null)
                    {
                        AdaptSelectionPanel();
                        m_SelectionPlayerPanel.SetActive(true);
                    }
                    else
                    {
                        m_Using = true;
                        StartUsingItem();
                    }
                }

                if (m_UseOnOnePlayer && m_PlayerSelected != null)
                {
                    m_Using = true;
                    StartUsingItem();
                }
            }
            
            if (m_Using)
                UpdateUsingItem();
            else
                UpdateUnusingItem();
        }
    }
    #endregion

    #region ItemSingleUse abstract & virtual methods
    protected abstract KeyCode GetUseKeyCode();

    protected virtual void StartUsingItem()
    {
        PlayUsedSound();
    }
    protected virtual void UpdateUsingItem() {}
    protected virtual void UpdateUnusingItem() { }

    protected virtual bool CheckUsingItemAllowed()
    {
        return true;
    }

    protected virtual void Set_UseOnOnePlayer()
    {
        m_UseOnOnePlayer = false;
    }

    protected virtual void AdaptSelectionPanel()
    {

    }

    protected void PlayUsedSound()
    {
        if (m_AudioSource != null && m_UsedSound != null)
        {
            m_AudioSource.clip = m_UsedSound;
            m_AudioSource.Play();
        }
    }
    #endregion
}
