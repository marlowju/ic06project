﻿using System;
using UnityEngine;

public abstract class ItemMultipleUse : Item
{
    #region Fields
    protected bool m_Using;
    protected DateTime m_LastChangeTime;

    protected bool m_UseOnOnePlayer = false;
    protected GameObject m_PlayerSelected = null;
    public GameObject m_SelectionPlayerPanel;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();
        m_LastChangeTime = DateTime.Now;
        m_Using = false;
        Set_UseOnOnePlayer();
        if (m_UseOnOnePlayer)
            m_SelectionPlayerPanel.SetActive(false);
        m_ItemSingleOrMultipleUse = "";
    }

    protected override void Update()
    {
        base.Update();

        if (m_Collected)
        {
            DateTime now = DateTime.Now;
            double time = (now - m_LastChangeTime).TotalSeconds;

            if (!m_Using)
            {
                if (CheckUsingItemAllowed() && Input.GetKey(GetUseKeyCode()) && time > 1)
                {
                    if (m_UseOnOnePlayer && m_PlayerSelected == null)
                    {
                        AdaptSelectionPanel();
                        m_SelectionPlayerPanel.SetActive(true);
                    }
                    else
                    {
                        m_LastChangeTime = DateTime.Now;
                        m_Using = true;
                        StartUsingItem();
                    }
                }

                if (m_UseOnOnePlayer && m_PlayerSelected != null)
                {
                    m_LastChangeTime = DateTime.Now;
                    m_Using = true;
                    StartUsingItem();
                }
            }
            else if (m_Using && CheckUnusingItemAllowed() && Input.GetKey(GetUseKeyCode()) && time > 1)
            {
                m_LastChangeTime = DateTime.Now;
                m_Using = false;
                m_PlayerSelected = null;
                StopUsingItem();
            }

            if (m_Using)
                UpdateUsingItem();
            else
                UpdateUnusingItem();
        }
    }
    #endregion

    #region ItemMultipleUse abstract & virtual methods
    protected abstract KeyCode GetUseKeyCode();

    protected virtual void StartUsingItem()
    {
        PlayUsedSound();
    }
    protected virtual void UpdateUsingItem() { }
    protected virtual void UpdateUnusingItem() { }
    protected virtual void StopUsingItem() { }

    protected virtual bool CheckUsingItemAllowed()
    {
        return true;
    }
    protected virtual bool CheckUnusingItemAllowed()
    {
        return true;
    }

    protected virtual void Set_UseOnOnePlayer()
    {
        m_UseOnOnePlayer = false;
    }

    protected virtual void AdaptSelectionPanel()
    {

    }

    protected void PlayUsedSound()
    {
        if (m_AudioSource != null && m_UsedSound != null)
        {
            m_AudioSource.clip = m_UsedSound;
            m_AudioSource.Play();
        }
    }
    #endregion

    #region Public methods
    public void HasAlreadyBeenCollected()
    {
        this.HasBeenCollected();
    }
    #endregion
}