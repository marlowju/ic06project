﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemManager : MonoBehaviour
{
    #region Fiels
    public GameObject m_Player1;
    public GameObject m_Player2;

    public ItemRope m_RopeItem;
    public ItemTrophee m_TropheeItem;
    #endregion

    #region Monobehaviour methods
    void Start()
    {
    }
    
    void Update()
    {
    }
    #endregion

    #region Public methods
    public bool HasTropheeBeenCollected()
    {
        return m_TropheeItem.IsCollected();
    }
    #endregion
}
