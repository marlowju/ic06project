﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ItemTrophee : Item
{
    protected override void Start()
    {
        base.Start();
        
        m_ItemName = "Ship piece";
        m_ItemUse = "Use it to repair your ship";
        m_ItemHowToUse = "Go back to your ship to repair it"+"\n"+"and leave this planet.";
        m_ItemSingleOrMultipleUse = "";
    }
}