﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ItemPickaxe : ItemMultipleUse
{
    #region Fields
    private Transform m_PlayerSelected_Transform;
    private SpriteRenderer m_PickaxeSpriteRenderer;
    private PlayerControler m_PlayerSelected_Controler;

    private PlayerControler m_Player1Controler;
    private PlayerControler m_Player2Controler;
    public Button m_Player1SelectionButton;
    public Button m_Player2SelectionButton;

    public Animator m_Animator;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();
        string name = GameObject.FindGameObjectWithTag("LevelManager").name;
        if (name.Equals("LevelManager3"))
            HasBeenCollected();
        else
            m_Collected = false;

        m_PickaxeSpriteRenderer = this.transform.Find("PickaxeUse").GetComponent<SpriteRenderer>();
        m_PickaxeSpriteRenderer.enabled = false;

        m_Player1Controler = this.GetComponentInParent<ItemManager>().m_Player1.GetComponent<PlayerControler>();
        m_Player2Controler = this.GetComponentInParent<ItemManager>().m_Player2.GetComponent<PlayerControler>();

        m_ItemName = "Pickaxe";
        m_ItemUse = "Use it to break stone or ice blocks";
        m_ItemHowToUse = "Press the P key to use the equip one player" + "\n" + "with the pickaxe." + "\n \n" + "Press the P key a second time to unequip." + "\n \n" + "Press the O key to use it.";

        m_Animator.enabled = false;
    }
    #endregion

    #region Override ItemMultipleUse methods
    protected override KeyCode GetUseKeyCode()
    {
        return KeyCode.P;
    }

    protected override void InitIsCollectable()
    {
        m_Collectable = false;
    }

    protected override void StartUsingItem()
    {
        base.StartUsingItem();
        UsingPickaxe();
        m_PlayerSelected_Controler.SetIsHoldingPickaxe(true);
        m_PickaxeSpriteRenderer.enabled = true;
        m_PickaxeSpriteRenderer.sortingLayerName = "OverPlayer";
    }

    protected override void UpdateUsingItem()
    {
        base.UpdateUsingItem();
        UsingPickaxe();

        if (Input.GetKey(KeyCode.O))
            m_Animator.enabled = true;
        else
            m_Animator.enabled = false;
    }

    protected override void StopUsingItem()
    {
        base.StopUsingItem();
        m_PickaxeSpriteRenderer.enabled = false;
        m_PlayerSelected_Controler.SetIsHoldingPickaxe(false);
        m_PlayerSelected = null;
        m_PlayerSelected_Transform = null;
        m_PlayerSelected_Controler = null;
        m_Animator.enabled = false;
    }

    protected override void Set_UseOnOnePlayer()
    {
        m_UseOnOnePlayer = true;
    }

    protected override void AdaptSelectionPanel()
    {
        base.AdaptSelectionPanel();
        if (m_Player1Controler.IsHoldingSomething())
            m_Player1SelectionButton.interactable = false;
        else
            m_Player1SelectionButton.interactable = true;

        if (m_Player2Controler.IsHoldingSomething())
            m_Player2SelectionButton.interactable = false;
        else
            m_Player2SelectionButton.interactable = true;
    }
    #endregion

    #region Pickaxe private methods
    private void UsingPickaxe()
    {
        this.transform.position = m_PlayerSelected_Transform.position;
        if (m_PlayerSelected_Controler.isFlipped())
        {
            this.transform.Translate(0.4f, 0, 0);
            if (!m_PickaxeSpriteRenderer.flipX)
            {
                Quaternion target = Quaternion.Euler(0, 0, 0);
                m_PickaxeSpriteRenderer.gameObject.transform.rotation = target;
            }
            m_PickaxeSpriteRenderer.flipX = true;
            m_Animator.SetBool("m_Flip", true);
            RotatePickaxe();
        }
        else
        {
            this.transform.Translate(-0.4f, 0, 0);
            if (m_PickaxeSpriteRenderer.flipX)
            {
                Quaternion target = Quaternion.Euler(0, 0, 0);
                m_PickaxeSpriteRenderer.gameObject.transform.rotation = target;
            }
            m_PickaxeSpriteRenderer.flipX = false;
            m_Animator.SetBool("m_Flip", false);
            RotatePickaxe();
        }
    }
    private void RotatePickaxe(float delta_angle = 0)
    {
        float x_position = this.transform.position.x;
        float y_position = this.transform.position.y;

        float rotationAngle = Mathf.Atan(Mathf.Abs(x_position) / Mathf.Abs(y_position)) * Mathf.Rad2Deg;
        if (x_position > 0)
            rotationAngle = -rotationAngle;
        if (y_position < 0)
            rotationAngle = 180 - rotationAngle;

        Quaternion target = Quaternion.Euler(0, 0, rotationAngle + delta_angle);
        this.transform.rotation = target;
    }
    #endregion

    #region UI
    public void OnPlayer1Selection()
    {
        m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player1;
        m_PlayerSelected_Transform = m_PlayerSelected.GetComponent<Transform>();
        m_PlayerSelected_Controler = m_PlayerSelected.GetComponent<PlayerControler>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnPlayer2Selection()
    {
        m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player2;
        m_PlayerSelected_Transform = m_PlayerSelected.GetComponent<Transform>();
        m_PlayerSelected_Controler = m_PlayerSelected.GetComponent<PlayerControler>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnCancelSelection()
    {
        m_PlayerSelected = null;
        m_PlayerSelected_Transform = null;
        m_PlayerSelected_Controler = null;
        m_SelectionPlayerPanel.SetActive(false);
    }
    #endregion
}