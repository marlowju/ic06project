﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ItemRope : ItemMultipleUse
{
    #region Fields
    private GameObject m_GOorigin;
    private GameObject m_GOtarget;

    private int m_Lenght;

    public DistanceJoint2D m_joint1;
    public DistanceJoint2D m_joint2;

    private LineRenderer m_RopeLine;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();
        string name = GameObject.FindGameObjectWithTag("LevelManager").name;
        if (name.Equals("LevelManager1"))
            m_Collected = false;
        else
        {
            HasBeenCollected();
        }

        m_GOorigin = this.GetComponentInParent<ItemManager>().m_Player1;
        m_GOtarget = this.GetComponentInParent<ItemManager>().m_Player2;
        m_Lenght = 4;

        m_joint1 = m_GOorigin.gameObject.GetComponent<DistanceJoint2D>();
        m_RopeLine = this.GetComponent<LineRenderer>();

        //Setting components to be sure everything is fine
        m_joint1.enabled = false;
        m_joint1.enableCollision = true;
        m_joint1.autoConfigureConnectedAnchor = false;
        m_joint1.connectedBody = m_joint2.gameObject.GetComponent<Rigidbody2D>();
        m_joint1.distance = m_Lenght / 2f;

        m_joint2.enabled = false;
        m_joint2.enableCollision = true;
        m_joint2.autoConfigureConnectedAnchor = false;
        m_joint2.connectedBody = m_GOtarget.GetComponent<Rigidbody2D>();
        m_joint2.distance = m_Lenght / 2f;

        m_RopeLine.enabled = false;

        m_ItemName = "Rope";
        m_ItemUse = "Use it to tie to each other";
        m_ItemHowToUse = "Press the R key to use the rope." + "\n" + "(make sure to be close enough to each other)." + "\n" + "\n" + "Press the R key a second time to remove it." + "\n" + "\n" + "Press the T key when the rope is taut to slowly rewind the rope.";
    }
    #endregion

    #region Override ItemMultipleUse methods
    protected override KeyCode GetUseKeyCode()
    {
        return KeyCode.R;
    }
    protected override bool CheckUsingItemAllowed()
    {
        bool result = (GetDistanceBetweenGO() <= m_Lenght);
        //+ check if there is any collider between two players
        return result;
    }

    protected override void StartUsingItem()
    {
        base.StartUsingItem();

        m_joint2.gameObject.transform.position = (m_GOorigin.transform.position + m_GOtarget.transform.position) / 2f;
        m_joint1.distance = m_Lenght / 2f;
        m_joint2.distance = m_Lenght / 2f;

        m_joint1.enabled = true;
        m_joint2.enabled = true;
        m_RopeLine.enabled = true;
        DrawRope();
    }

    protected override void UpdateUsingItem()
    {
        base.UpdateUsingItem();
        m_joint2.gameObject.transform.position = (m_GOorigin.transform.position + m_GOtarget.transform.position) / 2f;

        if (Input.GetKey(KeyCode.T))
        {
            float dt = Time.deltaTime;
            if (Mathf.Abs(m_GOorigin.transform.position.y) > Mathf.Abs(m_GOtarget.transform.position.y))
                m_joint2.distance += -0.1f * dt;
            else
                m_joint1.distance += -0.1f * dt;
        }

        DrawRope();
    }

    protected override void StopUsingItem()
    {
        base.StopUsingItem();
        m_joint1.enabled = false;
        m_joint2.enabled = false;
        m_RopeLine.enabled = false;
    }
    #endregion

    #region RopeItem private methods
    private float GetDistanceBetweenGO()
    {
        if (m_GOorigin == null)
            m_GOorigin = this.GetComponentInParent<ItemManager>().m_Player1;
        if (m_GOtarget == null)
            m_GOtarget = this.GetComponentInParent<ItemManager>().m_Player2;

        float xA = m_GOorigin.transform.position.x;
        float yA = m_GOorigin.transform.position.y;
        float xB = m_GOtarget.transform.position.x;
        float yB = m_GOtarget.transform.position.y;
        float distance = Mathf.Sqrt(Mathf.Pow(xB - xA, 2) + Mathf.Pow(yB - yA, 2));
        return distance;
    }

    private void DrawRope()
    {
        Vector3 start = m_GOorigin.transform.position;
        Vector3 end = m_GOtarget.transform.position;
        Vector3 tmp = new Vector3();

        m_RopeLine.positionCount = 9;
        m_RopeLine.enabled = true;
        m_RopeLine.startWidth = 0.1f;
        m_RopeLine.endWidth = 0.1f;
        m_RopeLine.SetPosition(0, start);
        for (int i=1; i<8; i++)
        {
            tmp.x = m_GOorigin.transform.position.x + i*(m_GOtarget.transform.position.x - m_GOorigin.transform.position.x) / 8;
            tmp.y = m_GOorigin.transform.position.y + i*(m_GOtarget.transform.position.y - m_GOorigin.transform.position.y) / 8;
            tmp.z = 0;
            m_RopeLine.SetPosition(i, tmp);

        }
        m_RopeLine.SetPosition(8, end);

    }
    #endregion
}
