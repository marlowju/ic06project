﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ItemJewel : ItemMultipleUse
{
    #region Fields
    private Transform m_PlayerSelected_Transform;
    public SpriteRenderer m_JewelSpriteRenderer;
    private PlayerControler m_PlayerSelected_Controler;

    private PlayerControler m_Player1Controler;
    private PlayerControler m_Player2Controler;
    public Button m_Player1SelectionButton;
    public Button m_Player2SelectionButton;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();
        m_JewelSpriteRenderer = this.transform.Find("JewelUse").GetComponent<SpriteRenderer>();
        m_JewelSpriteRenderer.enabled = false;

        m_Player1Controler = this.GetComponentInParent<ItemManager>().m_Player1.GetComponent<PlayerControler>();
        m_Player2Controler = this.GetComponentInParent<ItemManager>().m_Player2.GetComponent<PlayerControler>();

        m_ItemName = "Jewel";
        m_ItemUse = "Use it to distract some form of life";
        m_ItemHowToUse = "Press the J key to use the equip one player" + "\n" + "with the jewel." + "\n \n" + "Press the J key a second time to unequip.";
    }
    #endregion

    #region Override ItemMultipleUse methods
    protected override KeyCode GetUseKeyCode()
    {
        return KeyCode.J;
    }

    protected override void StartUsingItem()
    {
        base.StartUsingItem();
        UsingJewel();
        m_PlayerSelected_Controler.SetIsHoldingJewel(true);
        m_JewelSpriteRenderer.enabled = true;
        m_JewelSpriteRenderer.sortingLayerName = "OverPlayer";
    }

    protected override void UpdateUsingItem()
    {
        base.UpdateUsingItem();
        UsingJewel();
    }

    protected override void StopUsingItem()
    {
        base.StopUsingItem();
        m_JewelSpriteRenderer.enabled = false;
        m_PlayerSelected_Controler.SetIsHoldingJewel(false);
        m_PlayerSelected = null;
        m_PlayerSelected_Transform = null;
        m_PlayerSelected_Controler = null;
    }

    protected override void Set_UseOnOnePlayer()
    {
        m_UseOnOnePlayer = true;
    }

    protected override void AdaptSelectionPanel()
    {
        base.AdaptSelectionPanel();
        if (m_Player1Controler.IsHoldingSomething())
            m_Player1SelectionButton.interactable = false;
        else
            m_Player1SelectionButton.interactable = true;

        if (m_Player2Controler.IsHoldingSomething())
            m_Player2SelectionButton.interactable = false;
        else
            m_Player2SelectionButton.interactable = true;
    }
    #endregion

    #region Jewel private methods
    private void UsingJewel()
    {
        this.transform.position = m_PlayerSelected_Transform.position;
        if (m_PlayerSelected_Controler.isFlipped())
        {
            this.transform.Translate(0.4f, 0, 0);
            m_JewelSpriteRenderer.flipX = true;
            RotateJewel();
        }
        else
        {
            this.transform.Translate(-0.4f, 0, 0);
            m_JewelSpriteRenderer.flipX = false;
            RotateJewel();
        }
    }
    private void RotateJewel(float delta_angle = 0)
    {
        float x_position = this.transform.position.x;
        float y_position = this.transform.position.y;

        float rotationAngle = Mathf.Atan(Mathf.Abs(x_position) / Mathf.Abs(y_position)) * Mathf.Rad2Deg;
        if (x_position > 0)
            rotationAngle = -rotationAngle;
        if (y_position < 0)
            rotationAngle = 180 - rotationAngle;

        Quaternion target = Quaternion.Euler(0, 0, rotationAngle + delta_angle);
        this.transform.rotation = target;
    }
    #endregion

    #region UI
    public void OnPlayer1Selection()
    {
        m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player1;
        m_PlayerSelected_Transform = m_PlayerSelected.GetComponent<Transform>();
        m_PlayerSelected_Controler = m_PlayerSelected.GetComponent<PlayerControler>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnPlayer2Selection()
    {
        m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player2;
        m_PlayerSelected_Transform = m_PlayerSelected.GetComponent<Transform>();
        m_PlayerSelected_Controler = m_PlayerSelected.GetComponent<PlayerControler>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnCancelSelection()
    {
        m_PlayerSelected = null;
        m_PlayerSelected_Transform = null;
        m_PlayerSelected_Controler = null;
        m_SelectionPlayerPanel.SetActive(false);
    }
    #endregion
}