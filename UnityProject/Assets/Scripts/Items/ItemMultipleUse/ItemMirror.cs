﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ItemMirror : ItemMultipleUse
{
    #region Fields
    private SpriteRenderer m_MirrorSpriteRenderer;
    private CapsuleCollider2D m_MirrorCollider;
    private Transform m_PlayerSelected_Transform;
    private PlayerControler m_PlayerSelected_Controler;

    private PlayerControler m_Player1Controler;
    private PlayerControler m_Player2Controler;
    public Button m_Player1SelectionButton;
    public Button m_Player2SelectionButton;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();
        string name = GameObject.FindGameObjectWithTag("LevelManager").name;
        if (name.Equals("LevelManager3"))
            HasBeenCollected();
        else
            m_Collected = false;

        m_MirrorCollider = this.transform.Find("MirrorUse").GetComponent<CapsuleCollider2D>();
        m_MirrorSpriteRenderer = this.transform.Find("MirrorUse").GetComponent<SpriteRenderer>();

        m_MirrorSpriteRenderer.enabled = false;
        m_MirrorCollider.enabled = false;

        m_Player1Controler = this.GetComponentInParent<ItemManager>().m_Player1.GetComponent<PlayerControler>();
        m_Player2Controler = this.GetComponentInParent<ItemManager>().m_Player2.GetComponent<PlayerControler>();

        m_ItemName = "Mirror";
        m_ItemUse = "Use it to reflect light";
        m_ItemHowToUse = "Press the M key to equip one player" + "\n" + "with the mirror." + "\n \n" + "Press the M key a second time to unequip.";
    }
    #endregion

    #region Override ItemMultipleUse methods
    protected override KeyCode GetUseKeyCode()
    {
        return KeyCode.M;
    }

    protected override void StartUsingItem()
    {
        base.StartUsingItem();
        UsingMirror();
        m_PlayerSelected_Controler.SetIsHoldingMirror(true);
        m_MirrorSpriteRenderer.enabled = true;
        m_MirrorCollider.enabled = true;
    }

    protected override void UpdateUsingItem()
    {
        base.UpdateUsingItem();
        UsingMirror();
    }

    protected override void StopUsingItem()
    {
        base.StopUsingItem();
        m_MirrorSpriteRenderer.enabled = false;
        m_MirrorCollider.enabled = false;
        m_PlayerSelected_Controler.SetIsHoldingMirror(false);
        base.m_PlayerSelected = null;
        m_PlayerSelected_Transform = null;
        m_PlayerSelected_Controler = null;
    }

    protected override void Set_UseOnOnePlayer()
    {
        m_UseOnOnePlayer = true;
    }

    protected override void AdaptSelectionPanel()
    {
        base.AdaptSelectionPanel();
        if (m_Player1Controler.IsHoldingSomething())
            m_Player1SelectionButton.interactable = false;
        else
            m_Player1SelectionButton.interactable = true;

        if (m_Player2Controler.IsHoldingSomething())
            m_Player2SelectionButton.interactable = false;
        else
            m_Player2SelectionButton.interactable = true;
    }
    #endregion

    #region RopeItem private methods
    private void UsingMirror()
    {
        this.transform.position = m_PlayerSelected_Transform.position;
        if (m_PlayerSelected_Controler.isFlipped())
        {
            this.transform.Translate(0.5f, 0, 0);
            m_MirrorSpriteRenderer.flipX = false;
            RotateMirror(-15f);
        }
        else
        {
            this.transform.Translate(-0.5f, 0, 0);
            m_MirrorSpriteRenderer.flipX = true;
            RotateMirror(+15f);
        }
    }

    private void RotateMirror(float delta_angle = 0)
    {
        float x_position = this.transform.position.x;
        float y_position = this.transform.position.y;

        float rotationAngle = Mathf.Atan(Mathf.Abs(x_position) / Mathf.Abs(y_position)) * Mathf.Rad2Deg;
        if (x_position > 0)
            rotationAngle = -rotationAngle;
        if (y_position < 0)
            rotationAngle = 180 - rotationAngle;

        Quaternion target = Quaternion.Euler(0, 0, rotationAngle + delta_angle);
        this.transform.rotation = target;
    }
    #endregion

    #region UI
    public void OnPlayer1Selection()
    {
        base.m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player1;
        m_PlayerSelected_Transform = m_PlayerSelected.GetComponent<Transform>();
        m_PlayerSelected_Controler = m_PlayerSelected.GetComponent<PlayerControler>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnPlayer2Selection()
    {
        base.m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player2;
        m_PlayerSelected_Transform = m_PlayerSelected.GetComponent<Transform>();
        m_PlayerSelected_Controler = m_PlayerSelected.GetComponent<PlayerControler>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnCancelSelection()
    {
        base.m_PlayerSelected = null;
        m_PlayerSelected_Transform = null;
        m_PlayerSelected_Controler = null;
        m_SelectionPlayerPanel.SetActive(false);
    }
    #endregion
}

