﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemExplosif : ItemSingleUse
{
    #region Fields
    private bool m_Timer;
    private DateTime m_StartUsingTime;
    private int m_TimerLength; //in seconds
    private LineRenderer m_LineRenderer;

    public GameObject m_Wave;
    private Transform m_WaveTransform;
    private SpriteRenderer m_WaveRenderer;
    private BoxCollider2D m_WaveCollider;

    public GameObject m_ExplosifChrono;
    private Text m_ExplosifChronoText;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();

        m_LineRenderer = this.GetComponent<LineRenderer>();

        m_WaveTransform = m_Wave.GetComponent<Transform>();
        m_WaveRenderer = m_Wave.GetComponent<SpriteRenderer>();
        m_WaveCollider = m_Wave.GetComponent<BoxCollider2D>();

        m_ExplosifChronoText = m_ExplosifChrono.GetComponentInChildren<Text>();

        //Setting components to be sure everything is fine
        m_Timer = false;
        m_TimerLength = 5;
        m_LineRenderer.enabled = false;
        m_WaveRenderer.enabled = false;
        m_WaveCollider.enabled = false;
        m_ExplosifChrono.gameObject.SetActive(false);

        m_ItemName = "Explosive";
        m_ItemUse = "Use it to create a powerful wave";
        m_ItemHowToUse = "Press the B key to use the explosive.\n" + "You will have 5 seconds to rectify the wave's trajectory \n"+ "which propagates through the planet"+"\n \n" + "The wave created is so powerful that it can lift \n" + "massive objects on a small distance...";
    }
    #endregion

    #region ItemSingleUse abstract & virtual methods
    protected override KeyCode GetUseKeyCode()
    {
        return KeyCode.B;
    }

    protected override void StartUsingItem()
    {
        //base.StartUsingItem();
        m_Timer = true;
        m_StartUsingTime = DateTime.Now;
        m_LineRenderer.enabled = true;
        m_ExplosifChrono.gameObject.SetActive(true);
    }

    protected override void UpdateUsingItem()
    {
        base.UpdateUsingItem();
        if (m_Timer)
        {
            DrawLine();
            int nb_seconds_passed = (int)(DateTime.Now - m_StartUsingTime).TotalSeconds;
            m_ExplosifChronoText.text = GetTimerText(nb_seconds_passed);
            if (nb_seconds_passed > m_TimerLength)
            {
                PlayUsedSound();
                m_ExplosifChrono.gameObject.SetActive(false);
                m_Timer = false;
                m_LineRenderer.enabled = false;
                m_WaveTransform.position = m_PlayerSelected.transform.position;
                RotateWave();
                m_WaveRenderer.enabled = true;
                m_WaveCollider.enabled = true;
            }
        }
        else
        {
            if (GetWaveDistanceToCenterOfPlanet() > 13)
            {
                m_Using = false;
                m_Used = true;
                m_WaveRenderer.enabled = false;
                m_WaveCollider.enabled = false;
            }
            else
            {
                float dtime = Time.deltaTime;
                m_WaveTransform.Translate(0, -5 * dtime, 0);
            }
        }
    }

    protected override void Set_UseOnOnePlayer()
    {
        m_UseOnOnePlayer = true;
    }
    #endregion

    #region ItemExplosif private methods
    private float GetWaveDistanceToCenterOfPlanet()
    {
        float x_position = m_WaveTransform.position.x;
        float y_position = m_WaveTransform.position.y;
        float distance_to_center_planet = Mathf.Sqrt(Mathf.Pow(x_position, 2) + Mathf.Pow(y_position, 2));
        return distance_to_center_planet;
    }

    private void RotateWave()
    {
        float x_position = m_WaveTransform.position.x;
        float y_position = m_WaveTransform.position.y;

        float rotationAngle = Mathf.Atan(Mathf.Abs(x_position) / Mathf.Abs(y_position)) * Mathf.Rad2Deg;
        if (x_position > 0)
            rotationAngle = -rotationAngle;
        if (y_position < 0)
            rotationAngle = 180 - rotationAngle;

        Quaternion target = Quaternion.Euler(0, 0, rotationAngle);
        m_WaveTransform.rotation = target;
    }

    private void DrawLine()
    {
        Vector3 start = m_PlayerSelected.transform.position;
        Vector3 end = - m_PlayerSelected.transform.position;

        m_LineRenderer.positionCount = 2;
        m_LineRenderer.startWidth = 0.1f;
        m_LineRenderer.endWidth = 0.1f;
        m_LineRenderer.SetPosition(0, start);
        m_LineRenderer.SetPosition(1, end);
    }

    private String GetTimerText(int nb_seconds_passed)
    {
        int nb_seconds_toprint = m_TimerLength - nb_seconds_passed;
        String text = "";
        if (nb_seconds_toprint >= 0)
        {
            text = text + "00:";
            if (nb_seconds_toprint < 10)
                text = text + "0" + nb_seconds_toprint;
            else
                text = text + nb_seconds_toprint;
        }
        return text;
    }
    #endregion

    #region UI
    public void OnPlayer1Selection()
    {
        m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player1;
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnPlayer2Selection()
    {
        m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player2;
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnCancelSelection()
    {
        m_PlayerSelected = null;
        m_SelectionPlayerPanel.SetActive(false);
    }
    #endregion
}
