﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ItemExtinguisher : ItemSingleUse
{
    #region Fields
    public ParticleSystem m_ExtinguisherParticleSystem1;
    public ParticleSystem m_ExtinguisherParticleSystem2;
    private int m_TimerUsed; //in milliseconds
    private int m_TimerLength; //in milliseconds
    public GameObject m_ExtinguisherBar;
    private Image m_ExtinguisherBarImage;
    private Slider m_ExtinguisherSlider;

    private Transform m_PlayerSelected_Transform;
    private SpriteRenderer m_ExtinguisherSpriteRenderer;
    private PlayerControler m_PlayerSelected_Controler;

    private PlayerControler m_Player1Controler;
    private PlayerControler m_Player2Controler;
    public Button m_Player1SelectionButton;
    public Button m_Player2SelectionButton;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();
        m_TimerLength = 20 * 1000;
        m_TimerUsed = 0;

        m_ExtinguisherParticleSystem1.Stop();
        m_ExtinguisherParticleSystem1.gameObject.SetActive(false);
        m_ExtinguisherParticleSystem2.Stop();
        m_ExtinguisherParticleSystem2.gameObject.SetActive(false);

        m_ExtinguisherSpriteRenderer = this.transform.Find("ExtinguisherUse").GetComponent<SpriteRenderer>();
        m_ExtinguisherSpriteRenderer.enabled = false;

        m_Player1Controler = this.GetComponentInParent<ItemManager>().m_Player1.GetComponent<PlayerControler>();
        m_Player2Controler = this.GetComponentInParent<ItemManager>().m_Player2.GetComponent<PlayerControler>();

        m_ItemName = "Extinguisher";
        m_ItemUse = "Use it to freeze lava.";
        m_ItemHowToUse = "Press the G key to equip one player.\n" + "with the extinguisher\n \n" + "Press the F key to use it.";
    }
    #endregion

    #region Override ItemSingleUse methods
    protected override KeyCode GetUseKeyCode()
    {
        return KeyCode.G;
    }

    protected override void StartUsingItem()
    {
        base.StartUsingItem();
        m_ExtinguisherParticleSystem1.gameObject.SetActive(true);
        m_ExtinguisherParticleSystem2.gameObject.SetActive(true);

        m_ExtinguisherBarImage = m_ExtinguisherBar.GetComponentInChildren<Image>();
        m_ExtinguisherSlider = m_ExtinguisherBar.GetComponentInChildren<Slider>();

        m_ExtinguisherBarImage.sprite = this.m_ItemImage;
        m_ExtinguisherSlider.maxValue = m_TimerLength;
        m_ExtinguisherSlider.value = m_ExtinguisherSlider.maxValue;
        m_ExtinguisherBar.gameObject.SetActive(true);

        UsingExtinguisher();
        m_PlayerSelected_Controler.SetIsHoldingExtinguisher(true);
        m_ExtinguisherSpriteRenderer.enabled = true;
        m_ExtinguisherSpriteRenderer.sortingLayerName = "OverPlayer";
    }

    protected override void UpdateUsingItem()
    {
        base.UpdateUsingItem();
        UsingExtinguisher();
        if (Input.GetKey(KeyCode.F))
        {
            m_TimerUsed = (int)(m_TimerUsed + Time.deltaTime * 1000);
            if (m_PlayerSelected_Controler.isFlipped())
            {
                m_ExtinguisherParticleSystem1.Stop();
                m_ExtinguisherParticleSystem2.Play();
            }
            else
            {
                m_ExtinguisherParticleSystem2.Stop();
                m_ExtinguisherParticleSystem1.Play();
            }
        }
        else
        {
            m_ExtinguisherParticleSystem1.Stop();
            m_ExtinguisherParticleSystem2.Stop();
        }
        m_ExtinguisherSlider.value = m_TimerLength - m_TimerUsed;
        if (m_TimerUsed > m_TimerLength)
        {
            m_Using = false;
            m_Used = true;
            m_ExtinguisherBar.gameObject.SetActive(false);
            m_ExtinguisherSpriteRenderer.enabled = false;
            m_PlayerSelected_Controler.SetIsHoldingExtinguisher(false);
            m_PlayerSelected = null;
            m_PlayerSelected_Transform = null;
            m_PlayerSelected_Controler = null;
            m_ExtinguisherParticleSystem1.gameObject.SetActive(false);
            m_ExtinguisherParticleSystem2.gameObject.SetActive(false);
        }
    }

    protected override void Set_UseOnOnePlayer()
    {
        m_UseOnOnePlayer = true;
    }

    protected override void AdaptSelectionPanel()
    {
        base.AdaptSelectionPanel();
        if (m_Player1Controler.IsHoldingSomething())
            m_Player1SelectionButton.interactable = false;
        else
            m_Player1SelectionButton.interactable = true;

        if (m_Player2Controler.IsHoldingSomething())
            m_Player2SelectionButton.interactable = false;
        else
            m_Player2SelectionButton.interactable = true;
    }
    #endregion

    #region Extinguisher private methods

    #endregion
    private void UsingExtinguisher()
    {
        this.transform.position = m_PlayerSelected_Transform.position;
        if (m_PlayerSelected_Controler.isFlipped())
        {
            this.transform.Translate(0.4f, 0, 0);
            if (!m_ExtinguisherSpriteRenderer.flipX)
            {
                m_ExtinguisherSpriteRenderer.flipX = true;
            }
            RotateExtinguisher();
        }
        else
        {
            this.transform.Translate(-0.4f, 0, 0);
            if (m_ExtinguisherSpriteRenderer.flipX)
            {
                m_ExtinguisherSpriteRenderer.flipX = false;
            }
            RotateExtinguisher();
        }
    }

    private void RotateExtinguisher(float delta_angle = 0)
    {
        float x_position = this.transform.position.x;
        float y_position = this.transform.position.y;

        float rotationAngle = Mathf.Atan(Mathf.Abs(x_position) / Mathf.Abs(y_position)) * Mathf.Rad2Deg;
        if (x_position > 0)
            rotationAngle = -rotationAngle;
        if (y_position < 0)
            rotationAngle = 180 - rotationAngle;

        Quaternion target = Quaternion.Euler(0, 0, rotationAngle + delta_angle);
        this.transform.rotation = target;
    }

    #region UI
    public void OnPlayer1Selection()
    {
        base.m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player1;
        m_PlayerSelected_Controler = m_PlayerSelected.GetComponent<PlayerControler>();
        m_PlayerSelected_Transform = m_PlayerSelected.GetComponent<Transform>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnPlayer2Selection()
    {
        base.m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player2;
        m_PlayerSelected_Controler = m_PlayerSelected.GetComponent<PlayerControler>();
        m_PlayerSelected_Transform = m_PlayerSelected.GetComponent<Transform>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnCancelSelection()
    {
        base.m_PlayerSelected = null;
        m_PlayerSelected_Controler = null;
        m_PlayerSelected_Transform = null;
        m_SelectionPlayerPanel.SetActive(false);
    }
    #endregion

}

