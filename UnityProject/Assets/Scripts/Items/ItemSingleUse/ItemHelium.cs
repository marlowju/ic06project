﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ItemHelium : ItemSingleUse
{
    #region Fields
    private Rigidbody2D m_PlayerSelected_rb2d;

    private DateTime m_StartUsingTime;
    private int m_TimerLength; //in seconds
    public GameObject m_HeliumBar;
    private Image m_HeliumBarImage;
    private Slider m_HeliumSlider;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();
        m_HeliumBarImage = m_HeliumBar.GetComponentInChildren<Image>();
        m_HeliumSlider = m_HeliumBar.GetComponentInChildren<Slider>();

        m_TimerLength = 30;
        m_HeliumBar.gameObject.SetActive(false);

        m_ItemName = "Helium bottle";
        m_ItemUse = "Use it to challenge gravity.";
        m_ItemHowToUse = "Press the H key to use the helium bottle on one player.\n" + "The effect lasts 30 seconds.\n \n" + "Be careful, do not get lost in space...";
    }
    #endregion

    #region Override ItemSingleUse methods
    protected override KeyCode GetUseKeyCode()
    {
        return KeyCode.H;
    }

    protected override void StartUsingItem()
    {
        base.StartUsingItem();
        m_StartUsingTime = DateTime.Now;
        m_HeliumBarImage.sprite = this.m_ItemImage;
        m_HeliumSlider.maxValue = m_TimerLength;
        m_HeliumSlider.value = m_HeliumSlider.maxValue;
        m_HeliumBar.gameObject.SetActive(true);
    }

    protected override void UpdateUsingItem()
    {
        base.UpdateUsingItem();
        int nb_seconds_passed = (int)(DateTime.Now - m_StartUsingTime).TotalSeconds;
        m_HeliumSlider.value = m_TimerLength - nb_seconds_passed;
        if (nb_seconds_passed > m_TimerLength)
        {
            m_Using = false;
            m_Used = true;
            m_HeliumBar.gameObject.SetActive(false);
        }
        else
            InverseGravity();
    }

    protected override void Set_UseOnOnePlayer()
    {
        m_UseOnOnePlayer = true;
    }
    #endregion

    #region HeliumItem private methods
    private void InverseGravity()
    {
        float x_position = m_PlayerSelected.transform.position.x;
        float y_position = m_PlayerSelected.transform.position.y;
        m_PlayerSelected_rb2d.AddForce(Gravity.Instance.GetGravityVector(x_position, y_position, m_PlayerSelected_rb2d.mass) * -1.008f);
    }
    #endregion

    #region UI
    public void OnPlayer1Selection()
    {
        base.m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player1;
        m_PlayerSelected_rb2d = m_PlayerSelected.GetComponent<Rigidbody2D>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnPlayer2Selection()
    {
        base.m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player2;
        m_PlayerSelected_rb2d = m_PlayerSelected.GetComponent<Rigidbody2D>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnCancelSelection()
    {
        base.m_PlayerSelected = null;
        m_PlayerSelected_rb2d = null;
        m_SelectionPlayerPanel.SetActive(false);
    }
    #endregion
}

