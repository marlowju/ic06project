﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ItemAzote : ItemSingleUse
{
    #region Fields
    public PlayerControler m_PlayerSelected_Controler;
    public Button m_Player1SelectionButton;
    public Button m_Player2SelectionButton;

    private DateTime m_StartUsingTime;
    private int m_TimerLength; //in seconds
    public GameObject m_NitrogenBar;
    private Image m_NitrogenBarImage;
    private Slider m_NitrogenSlider;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();

        m_TimerLength = 30;

        m_ItemName = "Nitrogen bottle";
        m_ItemUse = "Use it to explore hot zones.";
        m_ItemHowToUse = "Press the N key to use the nitrogen bottle on one player.\n" + "The effect lasts 30 seconds.\n \n";
    }
    #endregion

    #region Override ItemSingleUse methods
    protected override KeyCode GetUseKeyCode()
    {
        return KeyCode.N;
    }

    protected override void StartUsingItem()
    {
        base.StartUsingItem();
        m_NitrogenBarImage = m_NitrogenBar.GetComponentInChildren<Image>();
        m_NitrogenSlider = m_NitrogenBar.GetComponentInChildren<Slider>();

        m_StartUsingTime = DateTime.Now;
        m_NitrogenBarImage.sprite = this.m_ItemImage;
        m_NitrogenSlider.maxValue = m_TimerLength;
        m_NitrogenSlider.value = m_NitrogenSlider.maxValue;
        m_NitrogenBar.gameObject.SetActive(true);
        m_PlayerSelected_Controler.SetCanRoast(false);
    }

    protected override void UpdateUsingItem()
    {
        base.UpdateUsingItem();
        int nb_seconds_passed = (int)(DateTime.Now - m_StartUsingTime).TotalSeconds;
        m_NitrogenSlider.value = m_TimerLength - nb_seconds_passed;
        if (nb_seconds_passed > m_TimerLength)
        {
            m_PlayerSelected_Controler.SetCanRoast(true);
            m_Using = false;
            m_Used = true;
            m_NitrogenBar.gameObject.SetActive(false);
            m_PlayerSelected = null;
            m_PlayerSelected_Controler = null;
        }
    }

    protected override void Set_UseOnOnePlayer()
    {
        m_UseOnOnePlayer = true;
    }

    protected override void InitIsCollectable()
    {
        m_Collectable = false;
    }
    #endregion

    #region UI
    public void OnPlayer1Selection()
    {
        base.m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player1;
        m_PlayerSelected_Controler = m_PlayerSelected.GetComponent<PlayerControler>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnPlayer2Selection()
    {
        base.m_PlayerSelected = this.GetComponentInParent<ItemManager>().m_Player2;
        m_PlayerSelected_Controler = m_PlayerSelected.GetComponent<PlayerControler>();
        m_SelectionPlayerPanel.SetActive(false);
    }

    public void OnCancelSelection()
    {
        base.m_PlayerSelected = null;
        m_PlayerSelected_Controler = null;
        m_SelectionPlayerPanel.SetActive(false);
    }
    #endregion

}

