﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ItemOxygen : ItemSingleUse
{
    #region Fields
    private DateTime m_StartUsingTime;
    private int m_TimerLength; //in seconds
    public GameObject m_OxygenBar;
    private Image m_OxygenBarImage;
    private Slider m_OxygenSlider;

    private PlayerControler m_PlayerControler1;
    private PlayerControler m_PlayerControler2;
    #endregion

    #region Override Monobehaviour methods
    protected override void Start()
    {
        base.Start();

        m_TimerLength = 30;

        m_ItemName = "Oxygen bottle";
        m_ItemUse = "Use it to explore underwater.";
        m_ItemHowToUse = "Press the Y key to use the oxygen bottle on both players.\n" + "The effect lasts 30 seconds.\n \n" + "Be careful, do not drown...";

        m_PlayerControler1 = this.GetComponentInParent<ItemManager>().m_Player1.GetComponent<PlayerControler>();
        m_PlayerControler2 = this.GetComponentInParent<ItemManager>().m_Player2.GetComponent<PlayerControler>();
    }
    #endregion

    #region Override ItemSingleUse methods
    protected override KeyCode GetUseKeyCode()
    {
        return KeyCode.Y;
    }

    protected override void StartUsingItem()
    {
        base.StartUsingItem();
        m_OxygenBarImage = m_OxygenBar.GetComponentInChildren<Image>();
        m_OxygenSlider = m_OxygenBar.GetComponentInChildren<Slider>();

        m_StartUsingTime = DateTime.Now;
        m_OxygenBarImage.sprite = this.m_ItemImage;
        m_OxygenSlider.maxValue = m_TimerLength;
        m_OxygenSlider.value = m_OxygenSlider.maxValue;
        m_OxygenBar.gameObject.SetActive(true);
        m_PlayerControler1.SetCanDrown(false);
        m_PlayerControler2.SetCanDrown(false);
    }

    protected override void UpdateUsingItem()
    {
        base.UpdateUsingItem();
        int nb_seconds_passed = (int)(DateTime.Now - m_StartUsingTime).TotalSeconds;
        m_OxygenSlider.value = m_TimerLength - nb_seconds_passed;
        if (nb_seconds_passed > m_TimerLength)
        {
            m_PlayerControler1.SetCanDrown(true);
            m_PlayerControler2.SetCanDrown(true);
            m_Using = false;
            m_Used = true;
            m_OxygenBar.gameObject.SetActive(false);
        }
    }
    #endregion

}

