﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    #region Constructor
    protected GameManager()
    {
        //Protected Constructor to prevent non-singleton constructor use.
    }
    #endregion

    [System.Serializable]
    public class GameState
    {
        public int m_LastLevelAchieved;

        public GameState()
        {
            m_LastLevelAchieved = 0;
        }

        public void Reset()
        {
            m_LastLevelAchieved = 0;
        }
    }

    private Scene m_GameScene;
    private Scene m_Level1Scene;
    private Scene m_Level2Scene;

    private GameState m_GameState;
    private int m_GeneralState;

    public Image m_BlackTransition_Image;
    public Animator m_BlackTransition_Animator;
    private IEnumerator m_BlackTransition_Coroutine;
    private bool m_ReloadingSceneProcess = false;

    #region UI binding
    private bool m_Restart = false;
    private bool m_Continue = false;
    public Button m_ContinueButton;
    public GameObject m_MainMenuPanel;
    public GameObject m_MainMenuBackground;
    #endregion

    private LevelManager1 m_LevelManager1 = null;
    private LevelManager2 m_LevelManager2 = null;
    private LevelManager3 m_LevelManager3 = null;

    // Start is called before the first frame update
    void Start()
    {
        Load();
        if (m_GameState == null)
        {
            m_ContinueButton.interactable = false;
            m_GameState = new GameState();
        }
        DontDestroyOnLoad(this);
        setMenuVisible(true);
    }

    // Update is called once per frame
    void Update()
    {
        switch (m_GeneralState)
        {
            case 0: //In main menu
                if (!m_MainMenuPanel.activeSelf || !m_MainMenuBackground.activeSelf)
                    setMenuVisible(true);
                Load();
                if (m_GameState.m_LastLevelAchieved != 0)
                {
                    m_ContinueButton.interactable = true;
                }
                else
                    m_ContinueButton.interactable = false;

                if (m_Restart)
                {
                    m_Restart = false;
                    m_Continue = false;

                    StartCoroutine(BlackTransitionFading("Game", "Level 1"));
                    Gravity.Instance.SetPlanetRadius(10.7f);
                    Gravity.Instance.SetGravityDistanceLimit(17f);
                    m_GameState.m_LastLevelAchieved = 0;
                    Save();
                    m_GeneralState = 1;
                }
                else if (m_Continue)
                {
                    m_Restart = false;
                    m_Continue = false;

                    switch (m_GameState.m_LastLevelAchieved)
                    {
                        case 1:
                            StartCoroutine(BlackTransitionFading("Game", "Level 2"));
                            Gravity.Instance.SetPlanetRadius(10.7f);
                            Gravity.Instance.SetGravityDistanceLimit(17f);
                            m_GeneralState = 2;
                            break;

                        case 2:
                            StartCoroutine(BlackTransitionFading("Game", "Level 3"));
                            Gravity.Instance.SetPlanetRadius(10.7f);
                            Gravity.Instance.SetGravityDistanceLimit(17f);
                            m_GeneralState = 3;
                            break;

                        default:
                            break;
                    }
                }
                break;

            case 1: //In level 1
                if (!m_ReloadingSceneProcess && SceneManager.GetSceneByName("Level 1").isLoaded) //wait for the end of transition
                {
                    if (m_LevelManager1 == null)
                        m_LevelManager1 = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager1>();
                    if (!SceneManager.GetActiveScene().name.Equals("Level 1"))
                        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Level 1"));

                    if (m_LevelManager1.GoToNextLevel())
                    {
                        StartCoroutine(BlackTransitionFading("Level 1", "Level 2"));
                        Gravity.Instance.SetPlanetRadius(10.7f);
                        Gravity.Instance.SetGravityDistanceLimit(17f);
                        m_GameState.m_LastLevelAchieved = 1;
                        Save();
                        m_GeneralState = 2;
                    }
                    else if (m_LevelManager1.GoBackToMainMenu())
                    {
                        StartCoroutine(BlackTransitionFading("Level 1", "Game"));
                        m_GeneralState = 0;
                    }
                    else if (m_LevelManager1.Reset())
                    {
                        StartCoroutine(BlackTransitionFading("Level 1", "Level 1"));
                    }
                }
                break;

            case 2: //In level 2
                if (!m_ReloadingSceneProcess && SceneManager.GetSceneByName("Level 2").isLoaded) //wait for the end of transition
                {
                    if (m_LevelManager2 == null)
                        m_LevelManager2 = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager2>();
                    if (!SceneManager.GetActiveScene().name.Equals("Level 2"))
                        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Level 2"));

                    if (m_LevelManager2.GoToNextLevel())
                    {
                        StartCoroutine(BlackTransitionFading("Level 2", "Level 3"));
                        Gravity.Instance.SetPlanetRadius(10.7f);
                        Gravity.Instance.SetGravityDistanceLimit(17f);
                        m_GameState.m_LastLevelAchieved = 2;
                        Save();
                        m_GeneralState = 3;
                    }
                    else if (m_LevelManager2.GoBackToMainMenu())
                    {
                        StartCoroutine(BlackTransitionFading("Level 2", "Game"));
                        setMenuVisible(true);
                        m_GeneralState = 0;
                    }
                    else if (m_LevelManager2.Reset())
                    {
                        StartCoroutine(BlackTransitionFading("Level 2", "Level 2"));
                    }
                }
                break;

            case 3: //In level 3
                if (!m_ReloadingSceneProcess && SceneManager.GetSceneByName("Level 3").isLoaded) //wait for the end of transition
                {
                    if (m_LevelManager3 == null)
                        m_LevelManager3 = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager3>();
                    if (!SceneManager.GetActiveScene().name.Equals("Level 3"))
                        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Level 3"));

                    if (m_LevelManager3.GoToNextLevel())
                    {
                        StartCoroutine(BlackTransitionFading("Level 3", "Game"));
                        m_GameState.m_LastLevelAchieved = 3;
                        Save();
                        m_GeneralState = 0;
                    }
                    else if (m_LevelManager3.GoBackToMainMenu())
                    {
                        StartCoroutine(BlackTransitionFading("Level 3", "Game"));
                        setMenuVisible(true);
                        m_GeneralState = 0;
                    }
                    else if (m_LevelManager3.Reset())
                    {
                        StartCoroutine(BlackTransitionFading("Level 3", "Level 3"));
                    }
                }
                break;

            default:
                Console.WriteLine("State unsupported in GameManager");
                break;
        }
    }

    #region Load and save GameState methods
    private void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGame.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGame.gd", FileMode.Open);
            m_GameState = (GameState) bf.Deserialize(file);
            file.Close();
        }
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGame.gd");
        bf.Serialize(file, m_GameState);
        file.Close();
    }
    #endregion

    #region UI bindings
    public void onRestartButton()
    {
        m_Continue = false;
        m_Restart = true;
    }

    public void onContinueButton()
    {
        m_Restart = false;
        m_Continue = true;
    }

    private void setMenuVisible(bool visible)
    {
        m_MainMenuPanel.SetActive(visible);
        m_MainMenuBackground.SetActive(visible);
    }

    private IEnumerator BlackTransitionFading(String sceneToUnload, String sceneToLoad)
    {
        if (sceneToLoad == sceneToUnload)
            m_ReloadingSceneProcess = true;
        m_BlackTransition_Animator.SetBool("m_FadeOut", true);
        yield return new WaitUntil(() => m_BlackTransition_Image.color.a == 1);
        m_BlackTransition_Animator.SetBool("m_FadeOut", false);
        if (sceneToUnload != null)
        {
            if (sceneToUnload == "Game")
                setMenuVisible(false);
            else
                SceneManager.UnloadSceneAsync(sceneToUnload);
        }
        if (sceneToLoad != null)
        {
            if (sceneToLoad == "Game")
            {
                setMenuVisible(true);
                SceneManager.SetActiveScene(SceneManager.GetSceneByName("Game"));
            }
            else
                SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Additive);
        }
        m_BlackTransition_Animator.SetBool("m_FadeIn", true);
        yield return new WaitUntil(() => m_BlackTransition_Image.color.a == 0);
        m_BlackTransition_Animator.SetBool("m_FadeIn", false);
        if (sceneToLoad == sceneToUnload)
            m_ReloadingSceneProcess = false;
    }
    #endregion
}
