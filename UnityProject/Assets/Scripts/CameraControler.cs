﻿using UnityEngine;

public class CameraControler : MonoBehaviour
{
    #region Fields
    public GameObject player1;
    public GameObject player2;
    private Vector3 offset;

    public enum MODE
    {
        Normal,
        Large
    }
    public MODE m_mode;
    #endregion

    #region Monobehaviour methods
    void Start()
    {
        m_mode = MODE.Normal;
        offset = transform.position - (player1.transform.position + player2.transform.position)/2;
    }

    void LateUpdate()
    {
        float distance_between_players = CalculateDistanceBetweenPlayers();

        if (distance_between_players > 8)
            m_mode = MODE.Large;
        else
            m_mode = MODE.Normal;

        float dtime = Time.deltaTime;
        if (m_mode == MODE.Normal)
        {
            GoToPositionTarget(GetPositionTarget_Normal(), dtime);
            GoToRotationTarget(GetRotationTarget_Normal(), dtime);
        }
        else
        {
            GoToPositionTarget(GetPositionTarget_Large(), dtime);
            GoToRotationTarget(GetRotationTarget_Large(), dtime);
        }
    }
    #endregion

    #region Protected / private methods
    protected float CalculateDistanceBetweenPlayers()
    {
        float xA = player1.transform.position.x;
        float yA = player1.transform.position.y;
        float xB = player2.transform.position.x;
        float yB = player2.transform.position.y;
        float players_distance = Mathf.Sqrt(Mathf.Pow(xB - xA, 2) + Mathf.Pow(yB - yA, 2));
        return players_distance;
    }

    #region MODE Normal methods
    protected Vector3 GetPositionTarget_Normal()
    {
        return (player1.transform.position + player2.transform.position) / 2 + offset;
    }

    protected Quaternion GetRotationTarget_Normal()
    {
        float x_position = this.transform.position.x;
        float y_position = this.transform.position.y;

        float rotationAngle = Mathf.Atan(Mathf.Abs(x_position) / Mathf.Abs(y_position)) * Mathf.Rad2Deg;
        if (x_position > 0)
            rotationAngle = -rotationAngle;
        if (y_position < 0)
            rotationAngle = 180 - rotationAngle;

        Quaternion target = Quaternion.Euler(0, 0, rotationAngle);
        return target;
    }
    #endregion

    #region MODE Large methods
    protected Vector3 GetPositionTarget_Large()
    {
        return new Vector3(0, 0, -25);
    }

    protected Quaternion GetRotationTarget_Large()
    {
        Quaternion target = Quaternion.Euler(0, 0, 0);
        return target;
    }
    #endregion

    #region Transition methods
    protected void GoToPositionTarget(Vector3 target, float dtime)
    {
        float new_x = Mathf.Lerp(this.transform.position.x, target.x, dtime);
        float new_y = Mathf.Lerp(this.transform.position.y, target.y, dtime);
        float new_z = Mathf.Lerp(this.transform.position.z, target.z, dtime);
        this.transform.position = new Vector3(new_x, new_y, new_z);
    }

    protected void GoToRotationTarget(Quaternion angle_target, float dtime)
    {
        transform.rotation = Quaternion.Slerp(this.transform.rotation, angle_target, dtime);
    }
    #endregion
    #endregion
}
