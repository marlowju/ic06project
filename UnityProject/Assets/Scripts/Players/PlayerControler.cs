﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public abstract class PlayerControler : MonoBehaviour
{
    #region Fields
    private GameObject m_planet;
    protected Rigidbody2D m_rb2d;
    public SpriteRenderer m_Image;
    private BoxCollider2D m_BoxCollider;
    protected float m_speed = 3f;
    private bool m_IsOnGround;
    private bool m_IsCloseToShip;
    protected Vector3 m_MovementVector;
    protected Animator m_Animator;
    protected bool m_IsFlipped = false;

    #region Items
    private bool m_IsHoldingMirror = false;
    private bool m_IsHoldingPickaxe = false;
    private bool m_IsHoldingJewel = false;
    private bool m_IsHoldingExtinguisher = false;
    #endregion

    #region Stone Crater
    private bool m_IsAtTheBottomOfTheCrater = false;
    #endregion

    #region Frozen lake
    private bool m_IsOnIce = false;
    private DateTime m_LastCollisionEnter;
    #endregion

    #region WateredZone
    private bool m_IsInWater = false;
    private bool m_IsTotallyInWater = false;
    private bool m_CanDrown = true;
    private int m_ApnoeaLenghtTimeInSeconds = 5;
    private Nullable<DateTime> m_ApnoeaBeginning;
    private bool m_IsDrown = false;
    public Image m_ApnoeaCircularBar;
    public Color m_ApnoeaColorGood;
    public Color m_ApnoeaColorBad;
    #endregion

    #region HotZone
    private bool m_IsTotallyInHotZone = false;
    private bool m_CanRoast = true;
    private int m_RoastLenghtTimeInSeconds = 5;
    private Nullable<DateTime> m_RoastBeginning;
    private bool m_IsRoasted = false;
    public Image m_RoastCircularBar;
    public Color m_RoastColorGood;
    public Color m_RoastColorBad;
    #endregion

    #region Fire
    private bool m_IsBurned = false;
    #endregion

    #region Lava
    private bool m_IsDeadByLava = false;
    #endregion
    #endregion

    #region Monobehaviour methods
    void Start()
    {
        m_rb2d = GetComponent<Rigidbody2D>();
        m_BoxCollider = GetComponent<BoxCollider2D>();
        m_MovementVector = new Vector3(0, 0, 0);
        if (m_ApnoeaCircularBar != null)
            m_ApnoeaCircularBar.enabled = false;
        if (m_RoastCircularBar != null)
            m_RoastCircularBar.enabled = false;
        m_Animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (!LostInSpace())
        {
            if (!m_IsInWater)
            {
                ApplyGravity();
                //Updating Movement Vector
                m_MovementVector.Set(0, 0, 0);
                if (CanWalk())
                    Walk();
                this.transform.Translate(m_MovementVector);
            }
            else
            {
                m_MovementVector.Set(0, 0, 0);
                Swim();
                this.transform.Translate(m_MovementVector);
            }

            //Dealing cosmo rotation
            Rotate();

            //Dealing cosmo animation
            UpdateAnimator();
        }
    }

    #endregion

    #region Protected / private methods
    protected void UpdateAnimator()
    {
        if ((!m_IsOnGround && !m_IsInWater) || Vector3.Equals(m_MovementVector, new Vector3(0, 0)))
        {
            m_Animator.enabled = false;
        }
        else
            m_Animator.enabled = true;
    }

    protected void ApplyGravity(float coeff = 1)
    {
        float x_position = this.transform.position.x;
        float y_position = this.transform.position.y;
        m_rb2d.AddForce(Gravity.Instance.GetGravityVector(x_position, y_position, m_rb2d.mass));
    }

    protected void Rotate()
    {
        float x_position = this.transform.position.x;
        float y_position = this.transform.position.y;

        float rotationAngle = Mathf.Atan(Mathf.Abs(x_position) / Mathf.Abs(y_position)) * Mathf.Rad2Deg;
        if (x_position > 0)
            rotationAngle = -rotationAngle;
        if (y_position < 0)
            rotationAngle = 180 - rotationAngle;

        Quaternion target = Quaternion.Euler(0, 0, rotationAngle);
        this.transform.rotation = target;
    }

    protected abstract void Walk();
    protected abstract void Swim();

    private float GetDistanceToCenterOfPlanet()
    {
        float x_position = this.transform.position.x;
        float y_position = this.transform.position.y;
        float distance_to_center_planet = Mathf.Sqrt(Mathf.Pow(x_position, 2) + Mathf.Pow(y_position, 2));
        return distance_to_center_planet;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Planet"))
            m_IsOnGround = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("CrashedShip"))
            m_IsCloseToShip = true;
        if (collision.gameObject.CompareTag("WateredZone"))
            m_IsInWater = true;
        if (collision.gameObject.CompareTag("Lake"))
        {
            m_LastCollisionEnter = DateTime.Now;
            m_IsOnIce = true;

            if (Mathf.Abs(this.transform.position.y) > Mathf.Abs(this.transform.position.x))
                StartCoroutine(SlideOnIce(-1f));
            else
                StartCoroutine(SlideOnIce(+1f));
        }
        if (collision.gameObject.CompareTag("BottomCrater"))
            m_IsAtTheBottomOfTheCrater = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("WateredZone"))
        {
            PolygonCollider2D wateredZoneCollider = collision.gameObject.GetComponent<PolygonCollider2D>();
            if (wateredZoneCollider.bounds.Contains(m_BoxCollider.bounds.min) && wateredZoneCollider.bounds.Contains(m_BoxCollider.bounds.max) && !m_IsTotallyInWater)
            {
                m_IsTotallyInWater = true;
            }
            else if ((!wateredZoneCollider.bounds.Contains(m_BoxCollider.bounds.min) || !wateredZoneCollider.bounds.Contains(m_BoxCollider.bounds.max)) && m_IsTotallyInWater)
            {
                m_IsTotallyInWater = false;
                m_ApnoeaBeginning = null;
                m_ApnoeaCircularBar.enabled = false;
                m_ApnoeaCircularBar.fillAmount = 1;
            }

            if (m_IsTotallyInWater)
            {
                if (!m_CanDrown)
                {
                    m_ApnoeaBeginning = null;
                    m_ApnoeaCircularBar.enabled = false;
                    m_ApnoeaCircularBar.fillAmount = 1;
                }
                else
                {
                    m_ApnoeaCircularBar.enabled = true;
                    if (m_ApnoeaBeginning == null)
                        m_ApnoeaBeginning = DateTime.Now;
                    int nb_seconds_passed = (int)(DateTime.Now - m_ApnoeaBeginning)?.TotalSeconds;
                    float apnoea_ratio = (float)(m_ApnoeaLenghtTimeInSeconds - nb_seconds_passed) / m_ApnoeaLenghtTimeInSeconds;
                    m_ApnoeaCircularBar.fillAmount = apnoea_ratio;
                    if (apnoea_ratio >= 0.5f)
                        m_ApnoeaCircularBar.color = m_ApnoeaColorGood;
                    else
                        m_ApnoeaCircularBar.color = m_ApnoeaColorBad;
                    if (nb_seconds_passed >= m_ApnoeaLenghtTimeInSeconds)
                    {
                        m_IsDrown = true;
                        m_ApnoeaCircularBar.enabled = false;
                    }
                }
            }
        }

        if (collision.gameObject.CompareTag("HotZone"))
        {
            PolygonCollider2D hotZoneCollider = collision.gameObject.GetComponent<PolygonCollider2D>();
            if (hotZoneCollider.bounds.Contains(m_BoxCollider.bounds.min) && hotZoneCollider.bounds.Contains(m_BoxCollider.bounds.max) && !m_IsTotallyInHotZone)
            {
                m_IsTotallyInHotZone = true;
            }
            else if ((!hotZoneCollider.bounds.Contains(m_BoxCollider.bounds.min) || !hotZoneCollider.bounds.Contains(m_BoxCollider.bounds.max)) && m_IsTotallyInHotZone)
            {
                m_IsTotallyInHotZone = false;
                m_RoastBeginning = null;
                m_RoastCircularBar.enabled = false;
                m_RoastCircularBar.fillAmount = 1;
            }

            if (m_IsTotallyInHotZone)
            {
                if (!m_CanRoast)
                {
                    m_RoastBeginning = null;
                    m_RoastCircularBar.enabled = false;
                    m_RoastCircularBar.fillAmount = 1;
                }
                else
                {
                    m_RoastCircularBar.enabled = true;
                    if (m_RoastBeginning == null)
                        m_RoastBeginning = DateTime.Now;
                    int nb_seconds_passed = (int)(DateTime.Now - m_RoastBeginning)?.TotalSeconds;
                    float roast_ratio = (float)(m_RoastLenghtTimeInSeconds - nb_seconds_passed) / m_RoastLenghtTimeInSeconds;
                    m_RoastCircularBar.fillAmount = roast_ratio;
                    if (roast_ratio >= 0.5f)
                        m_RoastCircularBar.color = m_RoastColorGood;
                    else
                        m_RoastCircularBar.color = m_RoastColorBad;
                    if (nb_seconds_passed >= m_RoastLenghtTimeInSeconds)
                    {
                        m_IsRoasted = true;
                        m_RoastCircularBar.enabled = false;
                    }
                }
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Planet"))
            m_IsOnGround = false;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("CrashedShip"))
            m_IsCloseToShip = false;
        if (collision.gameObject.CompareTag("WateredZone"))
        {
            m_IsInWater = false;
            m_IsTotallyInWater = false;
            m_ApnoeaBeginning = null;
        }
        if (collision.gameObject.CompareTag("Lake"))
            m_IsOnIce = false;
        if (collision.gameObject.CompareTag("HotZone"))
        {
            m_IsTotallyInHotZone = false;
            m_RoastBeginning = null;
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.CompareTag("Fire"))
        {
            m_IsBurned = true;
        }
    }

    protected bool IsOnGround()
    {
        return m_IsOnGround;
    }

    private IEnumerator SlideOnIce(float signOfTranslation)
    {
        while (m_IsOnIce)
        {
            float dt = Time.deltaTime;
            this.transform.Translate(signOfTranslation * dt * m_speed * 1f, 0, 0);
            yield return null;
        }
    }
    #endregion

    #region Public accessors
    public bool CanWalk()
    {
        return !m_IsOnIce && !LostInSpace();
    }

    public bool LostInSpace()
    {
        return (GetDistanceToCenterOfPlanet() >= Gravity.Instance.GetGravityDistanceLimit());
    }

    public bool IsCloseToShip()
    {
        return m_IsCloseToShip;
    }

    public bool IsDrown()
    {
        return m_IsDrown;
    }

    public bool IsRoasted()
    {
        return m_IsRoasted;
    }

    public bool IsBurned()
    {
        return m_IsBurned;
    }

    public void SetIsBurned(bool burned)
    {
        m_IsBurned = burned;
    }

    public bool IsDeadByLava()
    {
        return m_IsDeadByLava;
    }

    public void SetIsDeadByLava(bool dead)
    {
        m_IsDeadByLava = dead;
    }

    public bool IsAtTheBottomOfTheCrater()
    {
        return m_IsAtTheBottomOfTheCrater;
    }

    public bool IsHoldingSomething()
    {
        return m_IsHoldingMirror || m_IsHoldingPickaxe || m_IsHoldingJewel || m_IsHoldingExtinguisher;
    }

    public void SetIsHoldingMirror(bool holding)
    {
        m_IsHoldingMirror = holding;
    }

    public bool IsHoldingMirror()
    {
        return m_IsHoldingMirror;
    }

    public void SetIsHoldingPickaxe(bool holding)
    {
        m_IsHoldingPickaxe = holding;
    }

    public bool IsHoldingPickaxe()
    {
        return m_IsHoldingPickaxe;
    }

    public void SetIsHoldingJewel(bool holding)
    {
        m_IsHoldingJewel = holding;
    }

    public bool IsHoldingJewel()
    {
        return m_IsHoldingJewel;
    }

    public void SetIsHoldingExtinguisher(bool holding)
    {
        m_IsHoldingExtinguisher = holding;
    }

    public bool IsHoldingExtinguisher()
    {
        return m_IsHoldingExtinguisher;
    }

    public void SetCanDrown(bool candrown)
    {
        m_CanDrown = candrown;
    }

    public void SetCanRoast(bool canroast)
    {
        m_CanRoast = canroast;
    }

    public bool isFlipped()
    {
        return m_IsFlipped;
    }
    #endregion

}
