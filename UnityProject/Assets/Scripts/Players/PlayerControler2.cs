﻿using UnityEngine;

public class PlayerControler2 : PlayerControler
{
    protected override void Walk()
    {
        float dt = Time.deltaTime;

        if (Input.GetKey(KeyCode.Q))
        {
            m_MovementVector += Vector3.left * m_speed * dt;
            m_IsFlipped = false;
            m_Image.flipX = false;
        }
        if (Input.GetKey(KeyCode.D))
        {
            m_MovementVector += Vector3.right * m_speed * dt;
            m_IsFlipped = true;
            m_Image.flipX = true;
        }
        if (Input.GetKey(KeyCode.Z) && IsOnGround())
        {
            Vector2 v = Gravity.Instance.GetGravityVector(this.transform.position.x, this.transform.position.y, 1);
            v.Normalize();
            m_rb2d.AddForce(-0.6f * v, ForceMode2D.Impulse);
        }
    }

    protected override void Swim()
    {
        float dt = Time.deltaTime;

        if (Input.GetKey(KeyCode.Q))
        {
            m_MovementVector += Vector3.left * m_speed / 4f * dt;
            m_Image.flipX = false;
        }
        if (Input.GetKey(KeyCode.D))
        {
            m_MovementVector += Vector3.right * m_speed / 4f * dt;
            m_Image.flipX = true;
        }
        if (Input.GetKey(KeyCode.Z))
        {
            m_MovementVector += Vector3.up * m_speed / 2f * dt;
        }
        if (Input.GetKey(KeyCode.S))
        {
            m_MovementVector += Vector3.down * m_speed / 4f * dt;
        }
    }
}
